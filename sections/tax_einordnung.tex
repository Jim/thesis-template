\newpage\null\newpage
\chapter{Taxonomische Einordnung von Constraints}\label{ch:taxonomy}
Im Folgenden werden mögliche Arten von Constraints für die Pfadgenerierung (Motion Planning), die Handlung und die Bewegung von Robotern analysiert, erläutert und in einer Taxonomie eingeordnet. Eine grafische Darstellung der Taxonomie ist in Abbildung~\ref{fig:taxonomie} zu sehen.

\begin{figure}
	\centering	
	\includegraphics[height=\textheight, width=\textwidth, keepaspectratio]{images/Taxonomie.pdf}	
	\caption{Grafische Darstellung der präsentierten Taxonomie}
	\label{fig:taxonomie}
\end{figure}


\section{Pfad-Constraints}
Der Pfad resultiert aus einem erfolgreichen Motion Planning. Die Berechnung eines möglichen Pfads hängt sowohl vom Arbeitsbereich des Roboters ab als auch von angehängten Objekten oder Teilpfaden beziehungsweise Wegpunkten, die abgefahren werden müssen.

\subsection{Arbeitsbereich}
Der Arbeitsbereich eines Roboters beschreibt alle vom Endeffektor erreichbaren Punkte im Raum~\cite{siciliano_springer_2008}. Dabei lässt er sich nochmals in einen primären und sekundären Arbeitsbereich unterteilen. Der primäre Bereich enthält alle Punkte, die vom Endeffektor aus jeder Richtung, mit beliebiger Orientierung erreicht werden können. Der sekundäre Bereich ist eine Obermenge des primären Bereichs und enthält alle überhaupt erreichbaren Punkte~\cite{gupta_nature_1986}.

Beschränkt wird der Arbeitsbereich durch den eigentlichen Wirkungsbereich des Manipulators, durch die Anforderung der Kollaboration mit anderen Robotern oder Menschen und durch sich im Arbeitsbereich befindende Hindernisse jeglicher Art. Im Folgenden wird genauer auf diese Einschränkungen eingegangen.

\subsubsection{Wirkungsbereich}
Der Wirkungsbereich eines Roboters beschreibt seinen theoretisch maximalen Arbeitsbereich. Er ergibt sich aus der Größe des Roboters und der maximalen Gelenkwerte. Die mechanischen Grenzen beschränken die Bewegungsfreiheit der einzelnen Gelenke und damit auch den Arbeitsbereich des Roboters.

\subsubsection{Kollaboration}
Die Kollaboration mit Menschen oder anderen Robotern resultiert in weiteren Constraints, die sowohl beim Entwurf der Arbeitsumgebung, als auch beim Motion Planning berücksichtigt werden müssen. Eine sichere Kollaboration kann auf verschiedene Wege erreicht werden.

\paragraph{Sicherheitszonen}Matthias et al.~\cite{matthias_safety_2011} beschreiben eine Arbeitsumgebung, in der sich Roboter und Mensch ohne weitere Sicherheitsmechanismen permanent einen Arbeitsbereich teilen und der Roboter zu jeder Zeit harmlos für den Menschen ist. Sie präsentieren ein Sicherheitskonzept, welches es ermöglicht die Pfadplanung und -ausführung durchzuführen, ohne dass der Arbeitsbereich durch weitere Sensoren überwacht werden muss. Dazu werden folgende Anforderungen an den Cobot gestellt:
\begin{enumerate}
	\item Mechanische Maßnahmen / Soft Robotics
	\begin{enumerate}
		\item Leichtbauweise mit der Nutzlast entsprechenden Drehmomenten
		\item Glieder und Aktuatoren aus verformbaren Materialien~\cite{soft_robotics_2018}
		\item Gepolsterte und abgerundete Oberflächen 
		\item \glqq Back-drivability\grqq{} - die Möglichkeit den Roboter trotz aktiver Bremsen manuell bewegen zu können
	\end{enumerate}
	\item Kontrollmaßnahmen
	\begin{enumerate}
		\item Limitation von Leistung und Geschwindigkeit
		\item Kollisionserkennung durch Überwachung der Motorströme
	\end{enumerate}
	\item Andere Maßnahmen wie die Berücksichtigung von weiteren Gefahrenquellen durch bestimmte Werkstücke und Werkzeuge

\end{enumerate}
Können diese Anforderungen nicht erfüllt werden, ist es notwendig den Arbeitsbereich zu überwachen, um auf die Anwesenheit eines Menschen oder anderen Roboters reagieren zu können. Eine allgemeine Möglichkeit, zur Modellierung einer Roboterapplikation, ist aus allen verfügbaren Informationen ein Weltmodell zu erstellen, welches alle Entitäten in der Umgebung des Roboters und den Roboter selbst enthält und ein Bewegungsmodell, welches alle Bewegungen beschreibt~\cite{tactile_internet_ceti}. Für Anwendungen, in denen es zu einer Überschneidung der Arbeitsbereiche kommen kann, können innerhalb des Weltmodells Sicherheitszonen definiert werden, die das Verhalten und die Bewegungsplanung des Roboters beeinflussen~\cite{tactile_internet_ceti}. George Michalos et al.~\cite{michalos_design_2015} definieren vier Strategien, wie auf das Auftreten eines neuen Hindernisses in einer solchen Sicherheitszone reagiert werden kann:

\begin{enumerate}
	\item Die Geschwindigkeit und Kraft wird limitiert, um die Verletzungsgefahr zu minimieren. Auch Yamada et al.~\cite{yamada_human-robot_1997} zeigten in einer Untersuchung zur menschlichen Schmerztoleranz, dass Kollisionen mit einer Kontaktkraft von bis zu $50 N$ für Mensch-Roboter Interaktionen praktikabel sind
	
	\item Alle Operationen und Bewegungen des Roboters werden gestoppt
	
	\item Eine Kollision wird vermieden, indem ein neuer, kollisionsfreier Pfad geplant wird
	
	\item Wurde eine Sicherheitszone außerhalb des aktiven Arbeitsbereichs definiert, kann der menschliche Arbeiter gewarnt werden, bevor dieser den Arbeitsbereich betritt und eine der ersten drei Reaktionen provoziert
\end{enumerate}



\paragraph{Verbotene Zonen}
Verbotene Zonen können als Spezialfall von Sicherheitszonen definiert werden. Sie führen bei Betreten nicht zu einer Änderung des Verhaltens, sondern dürfen gar nicht erst vom Roboter geschnitten werden, wodurch der Arbeitsbereich effektiv eingeschränkt wird. Zhang et al.~\cite{zhang_obstacle_2008} beschreiben einen Algorithmus, der Hindernisse als verbotene Zonen behandelt. Im Motion-Planning-Schritt werden alle Pfade, die eine solche Zone schneiden verworfen. Da Zonen eine Abstraktion der Hindernisse ist, können sie auch für Bereiche definiert werden, die zwar kein Hindernis darstellen, jedoch trotzdem nicht vom Roboter geschnitten werden sollen, wie der Arbeitsbereich eines Menschen oder eines anderen Roboters.

\paragraph{Verhaltensänderung bei Berührung}
Sind Überschneidungen der Arbeitsbereiche und Berührungen zwischen Mensch und Roboter gewünscht, so muss diese Berührung vom Roboter erkannt werden. Dies kann beispielsweise durch kapazitive Sensoren an der Oberfläche des Roboters~\cite{michalos_design_2015} oder durch Kraft- beziehungsweise Drehmomentsensoren~\cite{shujun_lu_human-robot_2005} geschehen. Bei Berührung stoppt der Roboter alle Operationen und lässt sich vom Menschen führen.

\paragraph{Permanenter sicherer Betrieb}
Die einfachste aber auch ineffizienteste Strategie ist ein permanenter sicherer Betrieb, indem der komplette Arbeitsbereich als eine Sicherheitszone definiert wird, was eine dauerhafte Beschränkung von Geschwindigkeit und Kraft und permanente Kollisionsvermeidung erfordert.

\subsection{Hindernisse}
Da das Ziel des Motion Planning das Erstellen einer kollisionsfreie Trajektorie ist, beschränken Hindernisse innerhalb des Arbeitsbereichs maßgeblich die gültigen Pfade.
Das Springer Handbook of Robotics~\cite[Kapitel 35.9]{siciliano_springer_2008} definiert eine Taxonomie der verschiedenen Darstellungsmöglichkeiten von Hindernissen und Algorithmen, diese zu vermeiden. Eine Visualisierung der Taxonomie ist in Abbildung~\ref{fig:obstacle_tax} dargestellt.
Im folgenden Abschnitt werden diese Möglichkeiten kurz erläutert.

\begin{figure}
	\centering	
	\includegraphics[height=\textheight, width=\textwidth, 		  	keepaspectratio]{images/Hindernis_Taxonomie.pdf}	
	\caption{Taxonomie der Vorgehensweisen zur Hindernisvermeidung nach~\cite[Kapitel 35.9]{siciliano_springer_2008}}
	\label{fig:obstacle_tax}
\end{figure}


\subsubsection{Ein-Schritt-Methoden}
Ein-Schritt-Methoden sind Techniken zur Hindernisvermeidung, die Sensordaten direkt zur Bewegungssteuerung verwenden, ohne dass weitere Zwischenberechnungen notwendig sind. Dazu zählen Methoden, die auf physikalischen Analogien basieren. Ein Beispiel für eine solche Analogie ist die Potenzialfeldmethode, wo der Roboter als Partikel unter dem Einfluss eines Kräftefeldes angesehen wird. Die Zielposition übt eine anziehende Kraft auf das Partikel aus während es von Hindernissen abgestoßen wird. Die resultierende Bewegung ergibt sich aus der Summe der einwirkenden Kräfte. Heuristische Methoden sind von klassischen Planungsmethoden abgeleitet~\cite[Kapitel 35.9]{siciliano_springer_2008}. Ein Beispiel wäre das in Abschnitt~\ref{ch:motion_planning} erklärte Sampling-Based Planning.


\subsubsection{Mehr-Schritt-Methoden}
Bei Mehr-Schritt-Methoden werden Zwischeninformationen berechnet, die verarbeitet werden müssen, um eine Bewegung zu erhalten. Diese Methoden sind zum einen solche, die eine Untermenge an Bewegungsrichtungen berechnen, wie das Vektor-Feld-Histogramm oder die Hindernis-Begrenzungs-Methode und solche, die eine Menge an Geschwindigkeitsreglungen berechnen, wie der dynamische Fensteransatz oder die Methode der Geschwindigkeitshindernisse. Nachfolgend werden die Grundkonzepte dieser Methoden kurz dargestellt.

\paragraph{Vektor-Feld-Histogramm}
Hindernisse werden durch Wahrscheinlichkeiten repräsentiert. Daher eignet sich diese Methode besonders gut bei Verwendung von Sensoren mit höherer Unsicherheit, wie Ultraschallsensoren.
Die Welt wird vom Roboter ausgehen in Sektoren aufgeteilt. Den Sektoren wird anschließend die Wahrscheinlichkeitsdichte zugeteilt, dass sich in ihnen ein Hindernis befindet. Dadurch entsteht ein Polarhistogramm. Die Sektoren mit der kleinsten Dichte, die gleichzeitig am nächsten zu dem Sektor sind, der das Ziel enthält, werden als Kandidaten ausgewählt. Liegt das Ziel innerhalb eines der Sektoren, wird dieser als Richtung gewählt. Liegt das Ziel außerhalb, wird der Sektor mit der geringsten Dichte gewählt ~\cite[Seite 841]{siciliano_springer_2008}.

\paragraph{Hindernis-Begrenzungs-Methode}
Im ersten Schritt werden Zwischenziele definiert, wenn das eigentliche Ziel nicht auf direktem Wege erreichbar ist. Diese Zwischenziele liegen entweder zwischen Hindernissen oder auf der Kante eines Hindernisses. Das Zwischenziel mit dem geringsten Abstand zum Ziel wird als neues Ziel festgelegt. Für jedes Hindernis wird eine Menge an unerwünschten Richtungen berechnet, die für eine erfolgreiche Hindernisvermeidung ungeeignet wären. Im letzten Schritt wird ein Kompromiss berechnet, in dem aus den erlaubten Richtungen die ausgewählt wird, die sich dem Ziel annähert, gleichzeitig aber einen möglichst großen Abstand zu den Hindernissen hat. Da die Bewegungsgeschwindigkeit invers proportional zum Abstand zu den Hindernissen ist, wird diese dadurch ebenfalls optimiert. Als effektiv hat sich diese Methode vor allem in beengten Räumen gezeigt~\cite[Seite 842]{siciliano_springer_2008}.

\paragraph{Dynamischer Fensteransatz}
Im ersten Schritt wird eine Menge an Kandidaten Geschwindigkeitsregelungen, die noch ein Abbremsen ermöglichen, bevor es zu einer Kollision kommt, und die ihren Zielwert in vorgegebener Zeit erreichen können. Im zweiten Schritt wird die Regelung gewählt, die eine gewichtete Summe aus der Annäherung an das Ziel, den Abstand zu Hindernissen und die ausführbare Geschwindigkeit maximiert. Der dynamische Fensteransatz eignet sich vor allem für Roboter mit langsamen dynamischen Reaktionsmöglichkeiten oder für die Arbeit mit hohen Geschwindigkeiten~\cite[Kapitel 35.9.4 ]{siciliano_springer_2008}.

\paragraph{Geschwindigkeitshindernisse}
Die Methode der Geschwindigkeitshindernisse folgt den selben Annahmen wie der dynamische Fensteransatz, mit dem Unterschied, dass auch die Geschwindigkeiten der Hindernisse berücksichtigt werden. Dadurch eignet sie sich besonders für dynamische Anwendungsfälle~\cite[Kapitel 35.9.5]{siciliano_springer_2008}.

\paragraph{}
Eine allgemein optimale  Methode lässt sich nicht festlegen. Diese hängt von den Anforderungen, der Geschwindigkeit des Roboters und der Hindernisse und der Unsicherheit über den Zustand der Welt ab.

\subsection{Teilpfade}
In einigen Anwendungsfällen, wie beispielsweise dem Schweißen, ist es notwendig, dass der Endeffektor des Roboters zwischen Start- und Zielposition einen vorgegebenen Pfad abfährt. Befindet sich der Teilpfad vollständig innerhalb des stationären Arbeitsbereichs, können seine Punkte einfach als Zwischenziele im Motion Planning berücksichtigt werden. Beim Einsatz von mobilen Manipulatoren kann der zu folgende Teilpfad auch außerhalb des aktuellen Arbeitsbereichs sein, da sich dieser durch die Bewegung im Raum verändert~\cite{oriolo_motion_2005}. 

\subsection{Angefügte Objekte}
Nimmt ein Roboter ein Objekt auf, vergrößert dieses Objekt die Geometrie beziehungsweise die Maße des Roboters. Wird das Objekt beim Motion Planning nicht berücksichtigt, kann dies bei den folgenden Bewegungen zu unerwarteten Kollisionen führen. Dazu kann die Geometrie des Objektes dem kinematischen Modell des Roboters angefügt werden, sodass das Objekt aus Sicht des Motion Planning Algorithmus ein Teil des Roboters wird~\cite{sucan_open_2012}. Aufgenommene Objekte beschränken daher ebenfalls die Menge der gültigen Trajektorien.

\subsection{Abstand}
In einem unbeschränkten Motion Planning gilt die Pfadplanung als erfolgreich, sobald diese ausführbar und kollisionsfrei ist. Gerade bei schnelleren Bewegungen kann es sinnvoll sein einen zusätzlichen Sicherheitsabstand zu anderen Objekten zu halten, da ansonsten schon kleine Ungenauigkeiten in der Beschreibung der Welt zu Kollisionen führen können~\cite{canny_kinodynamic_nodate}. Die Einhaltung dieses Abstandes kann auf zwei Arten realisiert werden:

\begin{enumerate}
	\item Padding des Roboters: Ein Sicherheitsabstand wird zu allen Objekten gehalten.
	
	\item Padding der Objekte: Ein Abstand zu einzelnen Objekten muss eingehalten werden. 
\end{enumerate}


\subsection{Orientierung}
Einige Aufgaben, wie das Schweißen, das Aufnehmen von Objekten und vor allem auch die Handhabung von mit Flüssigkeit gefüllten Behältern, erfordern eine Beschränkung der Orientierung des Endeffektors~\cite{chitta_moveitros_2012}. Dies Reduziert die Menge an möglichen Trajektorien, da der Endeffektor sich nicht von Hindernissen wegdrehen kann und die notwendige Drehung des Gelenks, um die Orientierung zu halten, außerhalb der Limits liegen kann. Ebenso kann die Natur des Arbeitsbereichs die Orientierung des Endeffektors beschränken. Beispielsweise könnte eine Engstelle zwischen Start und Ziel eine bestimmte Orientierung erfordern.

\section{Handlungs-Constraints}
Neben den physischen Bewegungen können auch die abstrakten Handlungen eines Roboters eingeschränkt werden. Diese Art von Constraints ist vor allem dann notwendig, wenn der Roboter selbst seine Handlungen plant. Sie ergeben sich aus der Art des Endeffektors und aus Endeffektor-unabhängigen Einschränkungen.  

\paragraph{Endeffektor-spezifische Constraints}
Abhängig von dem ausgerüsteten Endeffektor, kann eventuell nur eine Teilmenge aller verfügbaren Handlungen ausgeführt werden. Ein Roboter mit einem Schweißgerät als Endeffektor kann beispielsweise keine Handlungen ausführen, die das Greifen eines Objekts beinhalten. 

\paragraph{Endeffektor-unspezifische Constraints}
Sobald Aufgaben nicht mehr unabhängig voneinander sind, muss die Handlungswahl des Roboters dahingehend eingeschränkt werden, dass die Kausalität der Handlungen eingehalten wird. So darf es einem Roboter erst möglich sein ein Objekt abzustellen, nachdem er es aufgenommen hat oder eine erfolgreich abgeschlossene Handlung erst zu kommunizieren, nachdem sie tatsächlich ausgeführt wurde. Roman Froschauer und Rene Lindorfer~\cite{froschauer_roman_workflow-based_nodate} präsentieren einen Workflow-basierten Programmieransatz, in dem solche Vorbedingungen realisiert und visualisiert werden können. Neben den eigenen Handlungen, können dabei auch Handlungen anderer Akteure, wie die eines weiteren Roboters oder eines Menschen, als Vorbedingungen modelliert werden.

\section{Bewegungs-Constraints}
Die dritte Untergruppe der Constraints sind Beschränkungen in der Bewegung des Roboters. In Abgrenzung zu den Pfad-Constraints, die den Pfad schon während des Planungsschrittes beschränken, schränken Bewegungs-Constraints die physische Bewegung beziehungsweise die Ausführung der Trajektorie ein. Dazu gehören die Beschränkung der Beschleunigung, der Geschwindigkeit, der Orientierung und der Kraft. Diese werden in den folgenden Abschnitten näher erläutert.

\subsection{Geschwindigkeit}
Die Begrenzung der Geschwindigkeit ist elementar für einen sicheren Betrieb im kollaborativen Umfeld. Die maximal zulässige Geschwindigkeit ist laut ISO 15066~\cite{ISO_15066} abhängig von der Trägheit beziehungsweise der Masse des Roboters und der sich im Arbeitsbereich befindenden Körperregion des Menschen. Die oberen Grenzwerte sind in Abbildung~\ref{fig:v_max} dargestellt. Bei Kollisionen unterhalb dieser Grenzen soll es zwar zu leichten Verletzungen wie einem Bluterguss kommen können, schwerere Verletzungen, wie die Penetration der Haut oder Brüche, können dadurch aber verhindert werden. 

Neben der Begrenzung der Geschwindigkeit aus Sicherheitsgründen können auch aufgabenspezifische Anforderungen eine weitere Einschränkung erfordern. Eine solche Anforderung wäre zum Beispiel das Reißen einer Schweißnaht zu verhindern.

\begin{figure}
	\centering	
	\includegraphics[height=\textheight, width=\textwidth, 		  	keepaspectratio]{images/v_max.png}	
	\caption{Maximal zulässige Geschwindigkeiten des Roboters in Abhängigkeit seiner effektiven Masse~\cite{ISO_15066}}
	\label{fig:v_max}
\end{figure}


\subsection{Beschleunigung}
Das Bewegen von Objekten erfordert häufig auch die Einschränkung der Beschleunigung. Dies ist insbesondere beim Umgang mit Flüssigkeiten notwendig, um die Trägheit der Flüssigkeit berücksichtigen zu können und so ein unkontrolliertes Überschwappen zu vermeiden \cite{maderna_robotic_2018}.

\subsection{Kraft}
Die Beschränkung der Kraft kann auf zweierlei Weise erfolgen. Zum einen kann die Kraft beschränkt werden, mit der sich der Roboter bewegt und eventuell Objekte verschiebt und zum anderen kann die Kraft beschränkt werden, mit der der Endeffektor ein Objekt greift~\cite{force_control}. Letzteres ist vor allem bei nicht-soliden Objekten notwendig, die bei einer zu hohen Krafteinwirkung beschädigt werden könnten.

So wie die Geschwindigkeit limitiert wird, um die Auftrittskraft bei einer Kollision zu beschränken, kann auch die Kraft allgemein so weit beschränkt werden, dass die in der ISO 15066 gegebenen Grenzwerte nicht überschritten werden~\cite[Tabelle A.2]{ISO_15066}.  