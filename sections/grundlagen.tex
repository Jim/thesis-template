\chapter{Grundlagen}\label{ch:basics}
Einige der wesentlichen Begriffe dieser Arbeit besitzen keine eindeutige Definition. Daher werden diese Begriffe zunächst kurz erklärt und eine für diese Arbeit geltende Definition festgelegt.

\section{Roboter}

Das Cambridge Dictionary definiert einen Roboter im Allgemeinen als \glqq eine von einem Computer gesteuerte Maschine, die zur automatischen Ausführung von Aufgaben genutzt wird\grqq{}~\cite{noauthor_robot_nodate}. 

Diese Arbeit bezieht sich in erster Linie auf industrielle Roboter oder auch industrielle Manipulatoren. Die Bezeichnungen Roboter und Manipulator werden daher in dieser Arbeit synonym verwendet. Mechanisch sind sie als serielle Kette zu beschreiben, in der jedes Glied, bis auf das erste und letzte, mit zwei weiteren Gliedern über jeweils ein Gelenk verbunden ist~\cite{siciliano_springer_2008}. Typischerweise besitzen solche Manipulatoren einen Endeffektor, der zur Handhabung, Montage und Bearbeitung von Werkstücken konzipiert ist und entsprechend der zu erledigenden Aufgabe zu wählen ist.


\section{Cobots}
Roboter sind dem Menschen in vielen Bereichen deutlich überlegen. So sind sie durchgängig einsetzbar und arbeiten weitaus genauer, als es einem Menschen möglich wäre. Die Überwachung und Entscheidungsfindung obliegt jedoch oft noch dem Menschen. Gerade in Bereichen, die ein hohes Maß an individualisierten Arbeitsschritten enthalten, wie beim Zusammenbau von individualisierbaren Komponenten in der Automobilindustrie~\cite{michalos_design_2015}, ist es heute noch nicht praktikabel menschliche Arbeiter vollständig zu ersetzen. Um trotzdem auch die Vorteile des Einsatzes von Robotern auszunutzen, ist es wichtig eine Umgebung zu schaffen, in der Roboter und Menschen sich einen gemeinsamen Arbeitsbereich teilen~\cite{siciliano_springer_2008}.
In solchen kollaborativen Zellen ist es notwendig, dass der Roboter eine Reihe von Sicherheitsbestimmungen gerecht wird~\cite{ISO_15066} und in der Lage ist, auf unerwartetes Verhalten des Menschen zu reagieren. Indem er sein eigenes Verhalten entsprechend anpasst und dadurch Verletzungen verhindert, kann eine stets sichere Arbeitsumgebung gewährleistet werden~\cite{tactile_internet_ceti}. Erfüllt ein Roboter diese Vorgaben und kann für kollaborative Arbeiten mit Menschen eingesetzt werden, gilt er als Cobot.

\section{Pose}
Die Pose eines Roboters beschreibt seine Position und Orientierung im Raum~\cite{siciliano_springer_2008}. Im Raum definiert werden kann eine Pose im \glqq Joint Space\grqq{} und im \glqq Cartesian Space\grqq{}. Eine Definition im \glqq Joint Space\grqq{} ist vollständig, da in ihr der Wert jedes Gelenks definiert ist. In der Regel ist aber vor allem die Pose des Endeffektors von Interesse. Durch einer Vorwärtstransformation oder auch Vorwärtskinematik, kann diese Pose anhand der Gelenkwerte bestimmt werden. Eine Beschreibung im \glqq Cartesian Space\grqq{} ist nicht vollständig, da hier lediglich die Pose des Endeffektors, in Form von kartesischen Koordinaten, definiert ist~\cite{yu_chapter_2018}.
Für eine vollständige Beschreibung einer gültigen Pose muss daher noch eine Rückwärtstransformation berechnet werden. 

Rückwärtstransformation oder auch Inverskinematik bezeichnet die Berechnung einer Konfiguration der Gelenkwerte, die in der Pose des Endeffektors resultieren. Damit eine Lösung gefunden werden kann, muss der Zielzustand innerhalb des Arbeitsbereichs des Roboters liegen. Aber auch dann kann es sowohl keine als auch mehrere Lösungen geben~\cite{siciliano_springer_2008}.
Dadurch ist es möglich, dass im \glqq Joint Space\grqq{} mehrere Posen existieren, die der Definition im \glqq Cartesian Space\grqq{} genügen. 

\section{Pfad und Trajektorie}
Der Pfad und die Trajektorie beschreiben eine Folge von Posen im Raum, die vom Roboter eingenommen werden sollen. Die Trajektorie enthält darüber hinaus Zeitabhängigkeiten und damit Informationen über Geschwindigkeit und Beschleunigung. Die Pfadplanung geht der Trajektorienplanung daher stets voraus, auch wenn beide Schritte nicht zwangsläufig separat erfolgen müssen~\cite{carbone_path_2015}.

\section{Motion Planning}\label{ch:motion_planning}
Motion Planning beschreibt die Aufgabe eine kontinuierliche und kollisionsfreie Trajektorie von einem gegebenen Startzustand zu einem Zielzustand zu finden~\cite[Seite 109]{siciliano_springer_2008} \cite{sucan_open_2012}. 

Das Finden einer optimalen Trajektorie ist PSPACE-vollständig und daher schwer zu implementieren und rechnerisch zu lösen. In der Praxis hat sich daher, für die meisten Anwendungsfälle, der alternativer Ansatz des Sampling-Based Planning durchgesetzt. Dabei wird eine Menge an kollisionsfreien Konfigurationen im Arbeitsbereich erstellt und erweitert, bis eine Kette von Konfigurationen gefunden wird, die die Start- und Ziel-Pose miteinander verbindet. Zur Überprüfung der Kollisionsfreiheit, greift er auf einen vorhandenen Algorithmus zur Kollisionserkennung zurück. Die Schrittgröße zwischen den einzelnen Konfigurationen ist dabei möglichst klein zu wählen, da für die Bewegung zwischen den Konfigurationen keine weitere Kollisionserkennung erfolgt ~\cite[Kapitel 5.1.3]{siciliano_springer_2008}.
Diese Art Planer ist auch in der Lage eine Lösung für Systeme mit mehr als drei Freiheitsgraden zu finden~\cite[Kapitel 5.2]{siciliano_springer_2008}. Existiert eine Lösung, so wird diese in endlicher Zeit gefunden, jedoch kann die Nicht-Existenz einer Lösung nicht festgestellt werden, wodurch eine Abbruchbedingung notwendig wird, die nach einer bestimmten Zeit die Suche nach einer Trajektorie beendet, um eine Blockierung zu verhindern~\cite{sucan_open_2012}.

\section{Constraints}
Die physischen Eigenschaften des Roboters resultieren in der realen Welt immer in Einschränkungen (Constraints) für seine Bewegung. Diese lassen sich in globale und lokale Constraints unterteilen. Während globale Constraints sich aus den Hindernissen in der Welt und möglichen Gelenklimits der Roboter ergeben, limitieren lokale Constraints die Geschwindigkeit und Beschleunigung, aufgrund der kinematischen und dynamischen Eigenschaften, wie die Erhaltung von Drehimpulsen~\cite{siciliano_springer_2008}.

Neben den natürlich gegebenen Constraints, können dem Planer noch weitere Beschränkungen auferlegt werden. Dadurch ist es möglich das Verhalten und die Bewegung des Roboters an die aufgabenspezifischen Anforderungen anzupassen, die Aufgabe des Motion Planning aber weiterhin einem Algorithmus zu überlassen. 
Häufig wird bei Constraints auch zwischen holonomen und nicht-holonomen Constraints unterschieden. Erstere können mathematisch alleine durch Gleichungen beschrieben werden, die lediglich von Positionen im Raum und optional auch der Zeit abhängig sind. Für nicht-holonomen Constraints ist mindestens eine Variable neben der Position von dessen zeitlicher Ableitung abhängig~\cite{berenson_constrained_nodate}. 

Die Holonomie eines Constraints wird maßgeblich von der Art des Roboters bestimmt. In dieser Arbeit werden nur stationäre Manipulatoren betrachtet und keine mobilen Roboter. Bei stationären Manipulatoren können die meisten Constraints durch Gleichungen beschrieben werden, die ausschließlich von den Gelenkwerten abhängig sind~\cite[Kapitel 1.3.6]{siciliano_springer_2008}. Daher sind die in der nachfolgenden Taxonomie dargestellten Constraints, bis auf die Geschwindigkeit und Beschleunigung, als holonome Constraints einzuordnen.
 

